<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $user = User::find($id);
        $user->update($data);

        return $user;
    }

    public function destroy($id)
    {
        //
    }
}
