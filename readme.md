To run this API:

- after download the repo you must need configure the .env file with your user and database name

run these commands:

    composer update
    php artisan key:generate
    php artisan migrate
    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
    php artisan jwt:secret

and finally run the server with: php artisan serve

to test api

Signup - POST 127.0.0.1:8000/api/auth/signup

{
    "email": "pushstart@email.com",
    "password": "password",
    "url": "image.png"
}

this request will return a response like this:

{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9zaWdudXAiLCJpYXQiOjE1NjkyNzUzNzEsImV4cCI6MTU2OTI3ODk3MSwibmJmIjoxNTY5Mjc1MzcxLCJqdGkiOiJrMUQ1dXl5cWxnSGxPQ0tTIiwic3ViIjozLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.dk6SOslS0ud3x_BrwT4CMivDnsmje6l3EBUgZzHZPq4",
    "token_type": "bearer",
    "expires_in": 3600
}

save the access_token and get the logged user

Me - POST 127.0.0.1:8000/api/auth/me?token=YOURTOKEN

response: 

{
    "id": 3,
    "name": "dev",
    "email": "dev2@gmail.com",
    "email_verified_at": null,
    "created_at": "2019-09-23 21:49:29",
    "updated_at": "2019-09-23 21:49:29"
}

---------

get user by ID - GET 

 "url": "http://127.0.0.1:8000/api/user/{id}",
  "method": "GET",
  "headers": {
    "Authorization": "Bearer YOUR TOKEN",
    "User-Agent": "PostmanRuntime/7.17.1",
    "Accept": "*/*",
    "Cache-Control": "no-cache",
    "Postman-Token": "943e485e-5d52-4c24-9ce2-09a46344724c,561a26da-3f8f-480d-918f-7d7cf1bf8d6d",
    "Host": "127.0.0.1:8000",
    "Accept-Encoding": "gzip, deflate",
    "Connection": "keep-alive",
    "cache-control": "no-cache"
  }

---------

login - POST 127.0.0.1:8000/api/auth/login

{
    "email": "pushstart@email.com",
    "password": "password"
}

---------

update - PUT 127.0.0.1:8000/api/user/{id}

{
	"name": "dev",
    "url": "image.png",
    "email": "pushstart@email.com",
    "password": "password"
}

------------

logout - POST 127.0.0.1:8000/api/auth/logout?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xMjcuMC4wLjE6ODAwMFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU2OTMzMzk1MiwiZXhwIjoxNTY5MzM3NTUyLCJuYmYiOjE1NjkzMzM5NTIsImp0aSI6Ind4TzhSWEpORXJNbGJaN0oiLCJzdWIiOjMsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.JCOLVjmGm4pOxDpUW36UB_71Q7c9Zw2H_2u9GBvM6_Q

response:

{
    "message": "Successfully logged out"
}